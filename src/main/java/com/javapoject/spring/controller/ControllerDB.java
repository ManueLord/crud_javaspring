package com.javapoject.spring.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.*;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.javapoject.spring.interfzeService.InterfazPersonService;
import com.javapoject.spring.model.Person;

@Controller
@RequestMapping
public class ControllerDB {
	
	@Autowired
	private InterfazPersonService service;
	
	@GetMapping("/list")
	public String list(Model model) {
		List<Person> people = service.listAll();
		model.addAttribute("people", people);
		return "index";
	}
	
	@GetMapping("/new")
	public String addList(Model model) {
		model.addAttribute("person", new Person());
		return "addNewPerson";
	}
	
	@GetMapping("/update/{id}")
	public String update(@PathVariable int id, Model model) {
		Optional<Person> person = service.listId(id);
		model.addAttribute("person", person);
		return "addNewPerson";
	}
	
	@PostMapping("/save")
	public String save(@Valid Person p, Model model) {
		service.save(p);
		return "redirect:/list";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable int id, Model model) {
		service.delete(id);
		return "redirect:/list";
	}
}