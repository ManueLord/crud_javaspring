package com.javapoject.spring.interfaze;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.javapoject.spring.model.Person;

@Repository
public interface InterfazPerson extends CrudRepository<Person, Integer> {
	
}
