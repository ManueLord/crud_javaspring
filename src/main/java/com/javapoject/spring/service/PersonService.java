package com.javapoject.spring.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javapoject.spring.interfaze.InterfazPerson;
import com.javapoject.spring.interfzeService.InterfazPersonService;
import com.javapoject.spring.model.Person;

@Service
public class PersonService implements InterfazPersonService{

	@Autowired
	private InterfazPerson data;
	
	@Override
	public List<Person> listAll() {
		return (List<Person>)data.findAll();
	}

	@Override
	public Optional<Person> listId(int id) {
		return data.findById(id);
	}

	@Override
	public int save(Person p) {
		int res = 0;
		Person person = data.save(p);
		if (!person.equals(null))
			res = 1;
		return res;
	}

	@Override
	public void delete(int id) {
		data.deleteById(id);
	}

}
