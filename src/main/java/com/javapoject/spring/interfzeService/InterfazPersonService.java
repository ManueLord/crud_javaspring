package com.javapoject.spring.interfzeService;

import java.util.List;
import java.util.Optional;

import com.javapoject.spring.model.Person;

public interface InterfazPersonService {
	public List<Person>listAll();
	public Optional<Person>listId(int id);
	public int save(Person p);
	public void delete(int id);
}
