package com.javapoject.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudJavaspringApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudJavaspringApplication.class, args);
	}

}
