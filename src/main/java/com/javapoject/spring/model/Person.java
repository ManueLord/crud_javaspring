package com.javapoject.spring.model;

import javax.persistence.*;

@Entity
@Table(name = "person")
public class Person {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private int year;
	private String date;
	private String gender;
	
	public Person() {
		// TODO Auto-generated constructor stub
	}

	public Person(int id, String name, int year, String date, String gender) {
		super();
		this.id = id;
		this.name = name;
		this.year = year;
		this.date = date;
		this.gender = gender;
	 }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
}
